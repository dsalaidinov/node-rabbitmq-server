# Getting Started

## Available Scripts

In the project directory, you can run:

### `npm install`
### `npm run start_m1` for starting server
### `npm run start_m2` for starting M2 service
### `curl -X POST -H "Content-Type: application/json" -d '{"data": "test"}' http://localhost:3003/process`
