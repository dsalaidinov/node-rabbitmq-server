const express = require('express');
const app = express();
const amqp = require('amqplib');

app.use(express.json());

const rabbitMQUrl = 'amqp://localhost'; 
const results = {};

async function handleTaskResult(result, correlationId) {
  console.log('Received result from M2:', result);

  results[correlationId] = result;
}

app.post('/process', async (req, res) => {
  try {
    const connection = await amqp.connect(rabbitMQUrl);
    const channel = await connection.createChannel();
    const queueName = 'task_queue';
    const resultQueue = 'result_queue';

    await channel.assertQueue(queueName, { durable: true });
    await channel.assertQueue(resultQueue, { durable: true });
    
    const correlationId = Math.random().toString(36).slice(2);
    channel.sendToQueue(queueName, Buffer.from(JSON.stringify(req.body)), {
      persistent: true,
      correlationId: correlationId,
      replyTo: resultQueue,
    });

    const resultPromise = new Promise((resolve) => {
      const timeout = setTimeout(() => {
        resolve({ error: 'Result not received' });
      }, 5000);

      channel.consume(resultQueue, (msg) => {
        if (msg.properties.correlationId === correlationId) {
          const result = JSON.parse(msg.content.toString());
          handleTaskResult(result, correlationId);
          clearTimeout(timeout);
          resolve({ result });
        }
      }, { noAck: true });
    });

    setTimeout(() => {
      connection.close();
    }, 500);

    res.json({ message: 'Task added to RabbitMQ', correlationId });
  } catch (error) {
    console.error('Error occurred:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

app.get('/result/:correlationId', (req, res) => {
  const correlationId = req.params.correlationId;
  const result = results[correlationId];
  if (result) {
    res.json({ result });
  } else {
    res.status(404).json({ error: 'Result not available yet' });
  }
});

app.listen(3003, () => {
  console.log('M1 microservice listening on port 3003');
});
