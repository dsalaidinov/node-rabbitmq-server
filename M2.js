const amqp = require("amqplib");

const rabbitMQUrl = "amqp://localhost";

async function processTask(task) {
  try {
    return JSON.stringify(task);
  } catch (error) {
    console.error("Error occurred during task processing:", error);
    throw error;
  }
}

async function startProcessing() {
  try {
    const connection = await amqp.connect(rabbitMQUrl);
    const channel = await connection.createChannel();
    const queueName = "task_queue";
    await channel.assertQueue(queueName, { durable: true });

    console.log("M2 microservice waiting for tasks...");

    channel.consume(queueName, async (msg) => {
      const task = JSON.parse(msg.content.toString());
      console.log("Received task:", task);

      const result = await processTask(task);

      const resultQueue = msg.properties.replyTo;
      const correlationId = msg.properties.correlationId;
      channel.sendToQueue(resultQueue, Buffer.from(result.toString()), {
        correlationId: correlationId,
      });

      channel.ack(msg);
    });
  } catch (error) {
    console.error("Error occurred:", error);
  }
}

startProcessing();
